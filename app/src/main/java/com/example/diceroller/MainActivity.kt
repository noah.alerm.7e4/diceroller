package com.example.diceroller

import android.annotation.SuppressLint
import android.media.MediaPlayer
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import androidx.annotation.RequiresApi
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.google.android.material.snackbar.Snackbar

class MainActivity : AppCompatActivity() {
    //VARIABLES
    lateinit var rollButton: Button
    lateinit var backButton: Button
    lateinit var dice1: ImageView
    lateinit var dice2: ImageView
    lateinit var mp: MediaPlayer

    //DRAWABLE LIST
    val diceDrawables = arrayOf(R.drawable.dice_1, R.drawable.dice_2, R.drawable.dice_3,
        R.drawable.dice_4, R.drawable.dice_5, R.drawable.dice_6)

    //FUNCTIONS
    /**
     * This function is used to roll one of the dice.
     * @param dice Die to move
     */
    @SuppressLint("UseCompatLoadingForDrawables")
    fun rollDice(dice: ImageView, bothDice: Boolean) {
        //SOUND
        //Restarts the sound so it doesn't have to wait for it to finish to replay it.
        mp.seekTo(0)
        mp.start()

        //ANIMATION
        if (bothDice) {
            YoYo.with(Techniques.Pulse).duration(400).repeat(1).playOn(dice1)
            YoYo.with(Techniques.Pulse).duration(400).repeat(1).playOn(dice2)
        }
        else
            YoYo.with(Techniques.Tada).duration(500).repeat(1).playOn(dice)

        //RANDOM ROLL
        dice.setImageDrawable(getDrawable(diceDrawables[(1..6).random()-1]))
    }

    /**
     * This function is used to show a snackbar when both dice are 6.
     */
    @RequiresApi(Build.VERSION_CODES.M)
    @SuppressLint("UseCompatLoadingForDrawables")
    fun checkIf6() {
        if (dice1.drawable.constantState?.equals(getDrawable(diceDrawables[5])?.constantState)!! &&
            dice2.drawable.constantState?.equals(getDrawable(diceDrawables[5])?.constantState)!!) {
            //Toast.makeText(applicationContext, "JACKPOT!", Toast.LENGTH_SHORT).show()
            Snackbar.make(findViewById(R.id.myCoordinatorLayout), "JACKPOT!",
                Snackbar.LENGTH_SHORT).setBackgroundTint(getColor(R.color.red)).show()
        }
    }

    //ON CREATE
    @RequiresApi(Build.VERSION_CODES.M)
    @SuppressLint("UseCompatLoadingForDrawables")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //IDs
        rollButton = findViewById(R.id.roll_button)
        backButton = findViewById(R.id.back_button)
        dice1 = findViewById(R.id.dice1)
        dice2 = findViewById(R.id.dice2)

        //MEDIA PLAYER
        mp = MediaPlayer.create(this, R.raw.dice_roll_sound)

        //CLICK LISTENERS
        //Roll Button
        rollButton.setOnClickListener {
            //DICE 1
            rollDice(dice1, true)

            //DICE 2
            rollDice(dice2, true)

            //SNACKBAR / TOAST
            checkIf6()
        }


        //Back Button
        backButton.setOnClickListener {
            dice1.setImageDrawable(getDrawable(R.drawable.empty_dice))
            dice2.setImageDrawable(getDrawable(R.drawable.empty_dice))
        }

        //Dice 1
        dice1.setOnClickListener {
            rollDice(dice1, false)
            checkIf6()
        }

        //Dice 2
        dice2.setOnClickListener {
            rollDice(dice2, false)
            checkIf6()
        }
    }
}